from django.urls import path

from . import views
from .views import LikeView, LikeCountView

urlpatterns = [

    path('posts/', views.PostView.as_view({'get': 'list', 'post': 'create'})),
    path('posts/<int:pk>/', views.PostView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('api/users/', views.UserView.as_view({'get': 'list', 'post': 'create'})),
    path('api/users/<int:pk>/', views.UserView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('like/', LikeView.as_view({'get': 'list', 'post': 'create', 'delete': 'destroy'})),
    path('user_like/analitics/', LikeCountView.as_view({'get': 'list'})),

]
