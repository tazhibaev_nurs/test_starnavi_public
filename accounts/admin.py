from django.contrib import admin
from django.contrib.auth.models import User
from simple_history import register
from simple_history.admin import SimpleHistoryAdmin
from accounts.models import Post


class PostHistoryAdmin(SimpleHistoryAdmin):
    list_display = ['title', 'created']
    history_list_display = ['status']
    search_fields = ['title']


admin.site.register(Post, PostHistoryAdmin)
register(User, app=__package__)
